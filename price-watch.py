#!/usr/bin/python3

# TODO : getting xrp from gatehub.net,

import requests
import time
import sys
from datetime import datetime
from coinbase.wallet.client import Client
from btcmarkets import BTCMarkets
import settings
from quoine.client import Quoinex
import re
import logging.config
from forex_python.converter import CurrencyRates
import argparse


logging.config.dictConfig(settings.LOGGING)
percents_logger = logging.getLogger('price-watch')


class Fees:
    BM_FACTOR = 100000000
    COINBASE_FEE = 0.0399
    COINSPOT_FEE = 0.003
    QUOINE_FEE = 0
    BTCMARKET_FEES = 0.007
    TX_FEES_TOTAL = 0.0474


CAPTAL = 5000
DELAY = 3


def get_coinspot_price(coin):
    try:
        r = requests.get(f'https://www.coinspot.com.au/buy/{coin}')
        price = re.search("(?<=marketRate = ).*(?=;\s)", r.text)[0]
        return round(float(price), 2)
    except Exception as e:
        print("Error while getting Coinspot price: {}".format(e))


def get_coinbase_price(coin):
    cb_client = Client(settings.COINBASE['API_KEY'], settings.COINBASE['API_SECRET'], api_version='2017-12-14')
    accounts = cb_client.get_accounts()
    cb_account_id = accounts.data[0]['id']
    pms = cb_client.get_payment_methods()
    cb_pay_method_id = pms.data[0]['id']
    buy = cb_client.buy(cb_account_id, payment_method=cb_pay_method_id, amount='1', currency=coin, commit='false',
                        quote='true')
    return float(buy['total']['amount'])


def get_bitstamp_price(coin):
    try:
        r = requests.get(f'https://www.bitstamp.net/api/v2/order_book/{coin}usd/')
        price = round(float(r.json()['bids'][0][0]), 2)
        c = CurrencyRates()
        bitstamp_price_aud = round(c.convert('USD', 'AUD', price), 2)
        return bitstamp_price_aud
    except Exception as e:
        print("Error while getting bitstamp price {}".format(e))


######### others ##########
def get_quoine_eth_price():
    try:
        quoine_client = Quoinex(settings.QUOINE['API_ID'], settings.QUOINE['API_SECRET'])
        aud_eth_price = quoine_client.get_order_book(product_id=33)['sell_price_levels'][0][0]
        return float(aud_eth_price)
    except Exception as e:
        print("Error while getting quoine price: {}".format(e))


def get_independentreserve_eth_price():
    try:
        r = requests.get(
            'https://api.independentreserve.com/Public/GetOrderBook?primaryCurrencyCode=eth&secondaryCurrencyCode=aud'
        )
        price = r.json()['BuyOrders'][0]['Price']
        return float(price)
    except Exception as e:
        print("Error while getting independentreserve price {}".format(e))


# BTCMarkets - The Seller!
def get_btcmarkets_sell_price(coin, coin_amount=1):
    bm_client = BTCMarkets(settings.BTCMARKETS['API_KEY'], settings.BTCMARKETS['API_SECRET'])
    orders = bm_client.get_market_orderbook(coin, 'AUD')
    bids = orders['bids']
    gross_sell_price = 0
    i = 0
    while coin_amount > 0:
        if bids[i][1] < coin_amount:  # If we cannot fulfil the amount in this order
            gross_sell_price += bids[i][0] * bids[i][1]
            coin_amount -= bids[i][1]
            i += 1
        else:  # We can fulfil the remaining remaining coin amount
            gross_sell_price += bids[i][0] * coin_amount
            coin_amount -= coin_amount

    net_sell_price = gross_sell_price - gross_sell_price * Fees.BTCMARKET_FEES
    return round(net_sell_price, 2)


def get_args():
    parser = argparse.ArgumentParser(description='CryptoCurrency Price Monitor & Arbitrage Indicator')

    # Add arguments
    parser.add_argument('--eth', action='store_true', help='Get Ethereum ', required=False, default=False)

    parser.add_argument('--xrp', action='store_true', help='Get Ripple Prices', required=False, default=False)

    parser.add_argument('--btc', action='store_true', help='Get Bitcoin Prices', required=False, default=False)

    parser.add_argument('--bch', action='store_true', help='Get Bitcoin Cash Prices', required=False, default=False)

    parser.add_argument('--ltc', action='store_true', help='Get Litecoin Prices', required=False, default=False)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()

    return parser.parse_args()


def main():
    args = get_args()

    while True:
        try:
            if args.btc:
                btcmarkets_btc_price = get_btcmarkets_sell_price('BTC')
                bitstamp_btc_price = get_bitstamp_price('btc')
                coinspot_btc_price = get_coinspot_price('btc')
                coinbase_btc_price = get_coinbase_price('BTC')

                bitstamp_percentage_btc = round(((btcmarkets_btc_price - bitstamp_btc_price) / bitstamp_btc_price * 100), 2)
                cb_percentage_btc = round(((btcmarkets_btc_price - coinbase_btc_price) / coinbase_btc_price * 100), 2)
                coinspot_percentage_btc = round(((btcmarkets_btc_price - coinspot_btc_price) / coinspot_btc_price * 100), 2)

                print('[+] ***BTC*** {:%Y/%m/%d-%H:%M:%S} - BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | '
                      'Coinbase: {:.2f}% | Bitstamp: {:.2f}%'
                      .format(datetime.now(), btcmarkets_btc_price, coinspot_percentage_btc, cb_percentage_btc,
                              bitstamp_percentage_btc))

                percents_logger.info('****BTC*** BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | Coinbase: {:.2f}% | '
                                     'Bitstamp: {:.2f}%'
                                     .format(btcmarkets_btc_price, coinspot_percentage_btc, cb_percentage_btc,
                                             bitstamp_percentage_btc))
        except Exception:
            print('Failed to get btc prices. Continuing...')

        try:
            if args.ltc:
                btcmarkets_ltc_price = get_btcmarkets_sell_price('LTC')
                coinbase_ltc_price = get_coinbase_price('LTC')
                coinspot_ltc_price = get_coinspot_price('ltc')
                bitstamp_ltc_price = get_bitstamp_price('ltc')

                cb_percentage_ltc = round(((btcmarkets_ltc_price - coinbase_ltc_price) / coinbase_ltc_price * 100), 2)
                bitstamp_percentage_ltc = round(((btcmarkets_ltc_price - bitstamp_ltc_price) / bitstamp_ltc_price * 100), 2)
                coinspot_percentage_ltc = round(((btcmarkets_ltc_price - coinspot_ltc_price) / coinspot_ltc_price * 100), 2)

                print('[+] ***LTC*** {:%Y/%m/%d-%H:%M:%S} - BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | '
                      'Coinbase: {:.2f}% | Bitstamp: {:.2f}%'
                      .format(datetime.now(), btcmarkets_ltc_price, coinspot_percentage_ltc, cb_percentage_ltc,
                              bitstamp_percentage_ltc))

                percents_logger.info('***LTC*** BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | Coinbase: {:.2f}% | '
                                     'Bitstamp: {:.2f}%'
                                     .format(btcmarkets_ltc_price, coinspot_percentage_ltc, cb_percentage_ltc,
                                             bitstamp_percentage_ltc))
        except Exception:
            print('Failed to get ltc prices. Continuing...')

        try:
            if args.bch:
                btcmarkets_bch_price = get_btcmarkets_sell_price('BCH')
                bitstamp_bch_price = get_bitstamp_price('bch')
                coinspot_bch_price = get_coinspot_price('bch')
                coinbase_bch_price = get_coinbase_price('BCH')

                bitstamp_percentage_bch = round(((btcmarkets_bch_price - bitstamp_bch_price) / bitstamp_bch_price * 100), 2)
                coinspot_percentage_bch = round(((btcmarkets_bch_price - coinspot_bch_price) / coinspot_bch_price * 100), 2)
                cb_percentage_bch = round(((btcmarkets_bch_price - coinbase_bch_price) / coinbase_bch_price * 100), 2)

                print('[+] ***BCH*** {:%Y/%m/%d-%H:%M:%S} - BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | '
                      'Coinbase: {:.2f}% | Bitstamp: {:.2f}%'
                      .format(datetime.now(), btcmarkets_bch_price, coinspot_percentage_bch, cb_percentage_bch,
                              bitstamp_percentage_bch))

                percents_logger.info('***BCH*** BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | Coinbase: {:.2f}% | '
                                     'Bitstamp: {:.2f}%'
                                     .format(btcmarkets_bch_price, coinspot_percentage_bch, cb_percentage_bch,
                                             bitstamp_percentage_bch))
        except Exception:
            print('Failed to get bch prices. Continuing...')

        try:
            if args.eth:
                btcmarkets_eth_price = get_btcmarkets_sell_price('ETH')
                quoine_eth_price = get_quoine_eth_price()
                coinspot_eth_price = get_coinspot_price('eth')
                coinbase_eth_price = get_coinbase_price('ETH')
                bitstamp_eth_price = get_bitstamp_price('eth')
                independentreserve_eth_price = get_independentreserve_eth_price()

                cb_percentage_eth = round(((btcmarkets_eth_price - coinbase_eth_price) / coinbase_eth_price * 100), 2)
                bitstamp_percentage_eth = round(((btcmarkets_eth_price - bitstamp_eth_price) / bitstamp_eth_price * 100), 2)
                coinspot_percentage_eth = round(((btcmarkets_eth_price - coinspot_eth_price) /
                                                 coinspot_eth_price * 100), 2) - 3.99
                ir_percentage_eth = round(((btcmarkets_eth_price - independentreserve_eth_price) /
                                           independentreserve_eth_price * 100), 2)
                quoine_percentage_eth = round(((btcmarkets_eth_price - quoine_eth_price) / quoine_eth_price * 100), 2)

                print('[+] ***ETH*** {:%Y/%m/%d-%H:%M:%S} - BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | '
                      'Quoine: {:.2f}%  | Indepented Reserve: {:.2f}% | Coinbase: {:.2f}% | Bitstamp: {:.2f}% '
                      .format(datetime.now(), btcmarkets_eth_price, coinspot_percentage_eth, quoine_percentage_eth,
                              ir_percentage_eth, cb_percentage_eth, bitstamp_percentage_eth))

                percents_logger.info('***ETH*** BTCMarkets Sell Price: ${:.2f}, Coinspot: {:.2f}%, Quoine: {:.2f}%, '
                                     'Indepented Reserve: {:.2f}%, Coinbase: {:.2f}%, Bitstamp: {:.2f}%'
                                     .format(btcmarkets_eth_price, coinspot_percentage_eth, quoine_percentage_eth,
                                             ir_percentage_eth, cb_percentage_eth, bitstamp_percentage_eth))
        except Exception:
            print('Failed to get eth prices. Continuing...')

        try:
            if args.xrp:
                btcmarkets_xrp_price = get_btcmarkets_sell_price('XRP')
                coinspot_xrp_price = get_coinspot_price('xrp')
                bitstamp_xrp_price = get_bitstamp_price('xrp')

                bitstamp_percentage_xrp = round(((btcmarkets_xrp_price - bitstamp_xrp_price) / bitstamp_xrp_price * 100), 2)
                coinspot_percentage_xrp = round(((btcmarkets_xrp_price - coinspot_xrp_price) / coinspot_xrp_price * 100), 2)

                print('[+] ***XRP*** {:%Y/%m/%d-%H:%M:%S} - BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | '
                      'Bitstamp: {:.2f}%'
                      .format(datetime.now(), btcmarkets_xrp_price, coinspot_percentage_xrp, bitstamp_percentage_xrp))

                percents_logger.info('***XRP*** BTCMarkets Price: ${:.2f} -> Coinspot: {:.2f}% | Bitstamp: {:.2f}%'
                                     .format(btcmarkets_xrp_price, coinspot_percentage_xrp, bitstamp_percentage_xrp))
        except Exception:
            print('Failed to get xrp prices. Continuing...')


if __name__ == '__main__':
    try:
        main()

    except KeyboardInterrupt:
        print('\nOkay! Exiting.')
        sys.exit(0)
