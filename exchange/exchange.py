class Exchange:

    def __init__(self):
        """Used to initiate the class and define all properties needed to start performing all functions"""
        pass

    def get_net_buy_price(self, coin_amount=1.0):
        """
        Returns a float representing the dollar amount needed to buy the given number of coins. The price returned
        should already take into account all fees/levies. This price should be calculated from the Ask Order Book
        instead of relying on the last bid price.
        :param coin_amount: The amount of coins that would be bought
        :type coin_amount: float
        :return: The dollar amount needed to buy coin_amount of coins
        :rtype: float
        """
        raise NotImplementedError("Subclass must implement abstract method")

    def get_net_sell_price(self, coin_amount=1.0):
        """
        Returns a float representing the dollar amount that would be received selling the given number of coins. The
        price returned should already take into account all fees/levies. This price should be calculated from the Bid
        Order Book instead of relying on the last ask price.
        :param coin_amount: The amount of coins that would be bought
        :type coin_amount: float
        :return: The dollar amount needed to buy coin_amount of coins
        :rtype: float
        """
        raise NotImplementedError("Subclass must implement abstract method")

    def update_long_term_values(self):
        """
        This method will be called periodically (approx. every 30 minutes) and after each buy/sell action. It should be
        used to update things like dynamic fees, limits and any other value likely to change over time.
        """
        raise NotImplementedError("Subclass must implement abstract method")

    def create_sell_order(self, coin_amount, net_price_per_coin=None):
        """
        Creates a tentative market sell order, giving the number of coins to sell and net price per coin. If the
        exchange requires the gross (before fees) amount, it is up to the method implementation to calculate it. It
        should also check whether the sell order is possible in the current scenario.
        :param coin_amount: The amount of coins to sell
        :type coin_amount: float
        :param net_price_per_coin: The price per coin after all fees are taken out. This is sometimes required. If not,
        it can be left as the default of None
        :type net_price_per_coin: float or None
        """
        raise NotImplementedError("Subclass must implement abstract method")

    def create_buy_order(self, coin_amount, net_price_per_coin=None):
        """
        Creates a tentative market buy order, giving the number of coins to buy and net price per coin. If the exchange
        requires the gross (before fees) amount, it is up to the method implementation to calculate it. It should also
        check whether the buy order is possible in the current scenario.
        :param coin_amount: The amount of coins to sell
        :type coin_amount: float
        :param net_price_per_coin: The price per coin after all fees are taken out. This is sometimes required. If not,
        it can be left as the default of None
        :type net_price_per_coin: float or None
        """
        raise NotImplementedError("Subclass must implement abstract method")

    def commit_sell(self):
        """
        Actually sends the sell order. Since all checks should be done when the order is created, the caller will
        assume this method always makes a successful sell order. If the order fails, an error should still be returned
        but the caller will continue as though it was successful. The returned value should be the final, exact amount
        of dollars gained from the sell.
        :return: The real amount of dollars received as a result of the sell.
        :rtype: float
        """
        raise NotImplementedError("Subclass must implement abstract method")

    def commit_buy(self):
        """
        Actually sends the buy order. Since all checks should be done when the order is created, the caller will
        assume this method always makes a successful buy order. If the order fails, an error should still be returned
        but the caller will continue as though it was successful. The returned value should be the final, exact amount
        of dollars spent during the sell.
        :return: The real amount of dollars spend as a result of the buy.
        :rtype: float
        """
        raise NotImplementedError("Subclass must implement abstract method")

    def can_buy(self):
        """
        :return: True if a buy is ever possible from this exchange. Otherwise False
        :rtype: bool
        """
        pass

    def can_sell(self):
        """
        :return: True if a sell is ever possible from this exchange. Otherwise False
        :rtype: bool
        """
        raise NotImplementedError("Subclass must implement abstract method")
