import sys
from coinbase.wallet.client import Client
from coinbase.wallet.error import ValidationError
from btcmarkets import BTCMarkets
from datetime import datetime
from time import sleep
import logging.config
import settings
from pushbullet import push
import curses

logging.config.dictConfig(settings.LOGGING)
base_logger = logging.getLogger(__name__)
tx_logger = logging.getLogger('transactions')
tick_logger = logging.getLogger('ticks')


class Arbbot:
    BM_FACTOR = 100000000
    CB_BUY_FEE = 0.0399

    def __init__(self):
        self.cb_client = Client(settings.COINBASE['API_KEY'], settings.COINBASE['API_SECRET'], api_version='2017-12-14')
        self.bm_client = BTCMarkets(settings.BTCMARKETS['API_KEY'], settings.BTCMARKETS['API_SECRET'])
        accounts = self.cb_client.get_accounts()
        self.cb_account_id = accounts.data[0]['id']
        pms = self.cb_client.get_payment_methods()
        self.cb_pay_method_id = pms.data[0]['id']
        self.limit_total = float(pms.data[0]['limits']['buy'][0]['total']['amount'])
        self.limit_remaining = float(pms.data[0]['limits']['buy'][0]['remaining']['amount'])
        self.bm_sell_fee = self.update_bm_sell_fee()
        self.buy_order = None

    def get_net_buy_price(self):
        buy = self.cb_client.buy(self.cb_account_id, payment_method=self.cb_pay_method_id, amount='1', currency='ETH',
                                 commit='false', quote='true')
        return float(buy['total']['amount'])

    def get_net_sell_price(self, coin_amount):
        orders = self.bm_client.get_market_orderbook('ETH', 'AUD')
        bids = orders['bids']
        gross_sell_price = 0
        i = 0
        while coin_amount > 0:
            if bids[i][1] < coin_amount:  # If we cannot fulfil the amount in this order
                gross_sell_price += bids[i][0] * bids[i][1]
                coin_amount -= bids[i][1]
                i += 1
            else:  # We can fulfil the remaining remaining coin amount
                gross_sell_price += bids[i][0] * coin_amount
                coin_amount -= coin_amount

        net_sell_price = gross_sell_price - gross_sell_price * self.bm_sell_fee
        return round(net_sell_price, 2), round(gross_sell_price, 2)

    def update_bm_sell_fee(self):
        trading_fee = self.bm_client.account_trading_fee('ETH', 'AUD')
        self.bm_sell_fee = trading_fee['tradingFeeRate'] / Arbbot.BM_FACTOR
        return self.bm_sell_fee

    def update_buy_limits(self):
        pms = self.cb_client.get_payment_methods()
        self.limit_total = float(pms.data[0]['limits']['buy'][0]['total']['amount'])
        self.limit_remaining = float(pms.data[0]['limits']['buy'][0]['remaining']['amount'])
        return self.limit_remaining, self.limit_total

    def get_min_profit_percent(self):
        scale = settings.MIN_PROFIT_SCALE
        limit_left_percent = self.limit_remaining / self.limit_total
        i = 0
        # Find which points we are between
        while i < len(scale) - 1 and limit_left_percent <= scale[i]['limit_percent']:
            i += 1

        percent_between = (limit_left_percent - scale[i - 1]['limit_percent']) / \
                          (scale[i]['limit_percent'] - scale[i - 1]['limit_percent'])

        min_profit_percent = (scale[i]['min_profit_percent'] - scale[i - 1]['min_profit_percent']) * percent_between + \
                             (scale[i - 1]['min_profit_percent'])

        return min_profit_percent

    def get_buy_dollar_amount(self, price_diff):
        scale = settings.MIN_PROFIT_SCALE
        # Find which points we are between
        i = 0
        while price_diff >= scale[i]['min_profit_percent']:
            # If we get to the end, just choose whole limit
            i += 1
            if i >= len(scale):
                return self.limit_remaining

        percent_between = (price_diff - scale[i - 1]['min_profit_percent']) / \
                          (scale[i]['min_profit_percent'] - scale[i - 1]['min_profit_percent'])

        buy_dollar_amount = (scale[i]['buy_dollar_amount'] - scale[i - 1]['buy_dollar_amount']) * percent_between + \
                            scale[i - 1]['buy_dollar_amount']

        return buy_dollar_amount if buy_dollar_amount <= self.limit_remaining else self.limit_remaining

    def create_buy_order(self, dollar_amount_total, commit='false'):
        dollar_amount_subtotal = dollar_amount_total / (1 + Arbbot.CB_BUY_FEE)
        self.buy_order = self.cb_client.buy(self.cb_account_id, payment_method=self.cb_pay_method_id,
                                            amount=dollar_amount_subtotal, currency='AUD', commit=commit)
        return self.buy_order

    def commit_buy_order(self):
        if not self.buy_order:
            return False

        buy_id = self.buy_order['id']
        try:
            commit = self.cb_client.commit_buy(self.cb_account_id, buy_id)
        except ValidationError:
            self.buy_order = None
            raise Exception('Failed to commit buy. Price has changed.')

        self.buy_order = None
        return commit

    def create_sell_order(self, coin_amount, price_per_coin):
        price_per_coin = round(price_per_coin * 100) * int(Arbbot.BM_FACTOR / 100)  # This is a weird thing that BM does
        coin_amount = int(coin_amount * Arbbot.BM_FACTOR)
        order = self.bm_client.order_create('AUD', 'ETH', price_per_coin, coin_amount, 'ask', 'market', 'abc-cdf-1000')
        if order['success']:
            try:
                details = self.bm_client.order_detail([order['id']])
                start_time = datetime.utcnow()
                while details['orders'][0]['status'] != 'Fully Matched':
                    details = self.bm_client.order_detail([order['id']])
                    if (datetime.utcnow() - start_time).total_seconds() > 3:  # Timeout
                        raise TimeoutError()
                return details
            except Exception:
                raise TimeoutError()
        else:
            raise Exception(f'Failed to create sell order ({order["errorMessage"]})')

    def create_sell_order_test(self, coin_amount, price_per_coin):
        coin_amount2 = 0.05138250
        coin_amount -= coin_amount2
        price_per_coin2 = price_per_coin - 1.75
        test_order = {
            'errorCode': None,
            'errorMessage': None,
            'orders': [{
                'clientRequestId': None,
                'creationTime': 1513672375066,
                'currency': 'AUD',
                'errorMessage': None,
                'id': 1018028639,
                'instrument': 'ETH',
                'openVolume': 0,
                'orderSide': 'Ask',
                'ordertype': 'Market',
                'price': int(price_per_coin * Arbbot.BM_FACTOR),
                'status': 'Fully Matched',
                'trades': [{
                    'creationTime': 1513672375109,
                    'description': None,
                    'fee': int(price_per_coin2 * coin_amount2 * self.bm_sell_fee * Arbbot.BM_FACTOR),
                    'id': 1018028662,
                    'orderId': 1018028639,
                    'price': int(price_per_coin2 * Arbbot.BM_FACTOR),
                    'side': 'Ask',
                    'volume': int(coin_amount2 * Arbbot.BM_FACTOR)
                }, {
                    'creationTime': 1513672375102,
                    'description': None,
                    'fee': int(price_per_coin * coin_amount * self.bm_sell_fee * Arbbot.BM_FACTOR),
                    'id': 1018028651,
                    'orderId': 1018028639,
                    'price': int(price_per_coin * Arbbot.BM_FACTOR),
                    'side': 'Ask',
                    'volume': int(coin_amount * Arbbot.BM_FACTOR)}],
                'volume': int((coin_amount + coin_amount2) * Arbbot.BM_FACTOR)}],
            'success': True}
        return test_order

    @staticmethod
    def get_gross_price_from_sell_order(sell_order):
        gross_sell_price = 0
        sell_fee = 0
        for trade in sell_order['orders'][0]['trades']:
            gross_sell_price += trade['price'] / Arbbot.BM_FACTOR * trade['volume'] / Arbbot.BM_FACTOR
            sell_fee += trade['fee'] / Arbbot.BM_FACTOR
        return round(gross_sell_price, 2), round(sell_fee, 2)


def check_settings():
    # Check the dynamic scale values
    scale = settings.MIN_PROFIT_SCALE
    if scale[0]['limit_percent'] != 1:
        raise ValueError('First point in scale must have limit_percent = 1')
    if scale[len(scale) - 1]['limit_percent'] != 0:
        raise ValueError('Last point in scale must have limit_percent = 0')
    for i in range(0, len(scale) - 1):
        if scale[i]['limit_percent'] <= scale[i + 1]['limit_percent']:
            raise ValueError(f'On the scale, Point{i+1} must have a > limit_percent value than Point{i+2}')
        if scale[i]['min_profit_percent'] > scale[i + 1]['min_profit_percent']:
            raise ValueError(f'On the scale, Point{i+1} must have a <= min_profit_percent value than Point{i+2}')

    if settings.TEST_MODE:
        base_logger.warning('Test mode is ON')


def init_screen():
    screen = curses.initscr()
    height, width = screen.getmaxyx()
    if height < 5 or width < 135:
        raise EnvironmentError('Console must be at least of size 135x5')

    screen.clear()
    screen.refresh()
    screen.border(0)
    curses.curs_set(0)
    curses.start_color()
    curses.init_pair(1, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
    return screen


def draw_screen(screen, ticks, transactions, errors, status):
    screen.clear()
    height, width = screen.getmaxyx()

    # print borders
    x_right_col = 82
    y_half = (height - 1) // 2
    screen.addstr(0, 0, u'\u2500' * width)
    screen.addstr(y_half, x_right_col, u'\u2500' * (width - x_right_col))
    for y in range(0, height):
        screen.addstr(y, x_right_col, u'\u2502')
    screen.addstr(0, x_right_col, u'\u252c')
    screen.addstr(y_half, x_right_col, u'\u251c')

    # print main title
    title = ' *** ARBBOT *** '
    start_x_title = int(x_right_col // 2 - len(title) // 2 - len(title) % 2)
    screen.attron(curses.color_pair(2))
    screen.attron(curses.A_BOLD)
    screen.addstr(0, start_x_title, title)
    screen.attroff(curses.color_pair(2))
    screen.attroff(curses.A_BOLD)

    # print errors title
    title = ' !!! Errors !!! '
    start_x_title = ((width - x_right_col) // 2 - len(title) // 2) + x_right_col
    screen.attron(curses.color_pair(1))
    screen.attron(curses.A_BOLD)
    screen.addstr(0, start_x_title, title)
    screen.attroff(curses.color_pair(1))
    screen.attroff(curses.A_BOLD)

    # print transactions title
    title = ' $$$ Transactions $$$ '
    start_x_title = ((width - x_right_col) // 2 - len(title) // 2) + x_right_col
    screen.attron(curses.color_pair(1))
    screen.attron(curses.A_BOLD)
    screen.addstr(y_half, start_x_title, title)
    screen.attroff(curses.color_pair(1))
    screen.attroff(curses.A_BOLD)

    # print status bar
    status_string = f'CB Limit: ${int(status["limit"])}/{int(status["limit_total"])} | ' \
                    f'Buy At: {round(status["buy_at"] * 100, 2)}% | ' \
                    f'Test Mode: {status["test_mode"]}'
    start_x_status = width // 2 - len(status_string) // 2
    screen.attron(curses.color_pair(3))
    screen.addstr(height - 1, 0, ' ' * (width - 1))
    screen.addstr(height - 1, start_x_status, status_string)
    screen.attroff(curses.color_pair(3))

    # print ticks
    if len(ticks) > height - 2:
        ticks = ticks[-(height - 2):]

    ycursor = height - len(ticks) - 1
    for tick in ticks:
        screen.addstr(ycursor, 0, tick)
        ycursor += 1

    # print transactions
    if len(transactions) > height - y_half - 2:
        transactions = transactions[-(height - y_half - 2):]

    ycursor = y_half + 1
    start_x_transactions = x_right_col + 1
    for tx in transactions:
        screen.addstr(ycursor, start_x_transactions, tx)
        ycursor += 1

    # print errors
    if len(errors) > y_half - 1:
        errors = errors[-(y_half - 1):]

    ycursor = 1
    start_x_transactions = x_right_col + 1
    for error in errors:
        screen.addstr(ycursor, start_x_transactions, error[:(width - x_right_col - 1)])
        ycursor += 1

    screen.refresh()


def exit_gracefully():
    base_logger.info('Exiting...')
    curses.curs_set(1)
    curses.endwin()
    sys.exit(0)


def main():
    try:
        base_logger.debug('Initialising Arbbot')
        check_settings()
        screen = init_screen()
        arbbot = Arbbot()
        last_update = datetime.utcnow()
    except Exception as e:
        base_logger.exception('Error initialising Arbbot')
        print(f'Fatal exception during initialisation: {e}')
        return
    error_count = 0
    main_console = []
    errors_console = []
    while True:
        try:
            # Note down start time
            start_time = datetime.utcnow()

            # Update limits if we haven't done so in the last 30 minutes
            if (datetime.utcnow() - last_update).total_seconds() > 60 * 30:
                base_logger.info('Updating buy limit and sell fees')
                arbbot.update_buy_limits()
                arbbot.update_bm_sell_fee()
                last_update = datetime.utcnow()

            # Grab buy and sell prices
            base_logger.info('Getting Buy/Sell prices')
            buy_price = arbbot.get_net_buy_price()
            sell_price, gross_sell_price_per_coin = arbbot.get_net_sell_price(1)
            diff = 1 - (buy_price / sell_price)
            base_logger.debug(f'buy_price={buy_price} sell_price={sell_price} '
                              f'gross_sell_price_per_coin={gross_sell_price_per_coin} diff={diff}')
            tick_logger.info(f'{diff * 100:5.2f}%,${buy_price:7.2f},${sell_price:7.2f}')
            main_console.append(f'{datetime.utcnow().isoformat(timespec="milliseconds")}Z [Price Check Diff] '
                                f'{diff * 100:5.2f}% | CB: ${buy_price:7.2f} | BM: ${sell_price:7.2f}')

            # Is it a good time to action?
            if diff >= arbbot.get_min_profit_percent():
                base_logger.info(f'Found good price ({diff * 100:.2f})! Let\'s do this...')
                main_console.append('Found good price! Let\'s do this...')

                # See if we have enough money to do an action
                if arbbot.limit_remaining < settings.HARD_MIN_SPEND:
                    base_logger.info(f'Not enough bananas (${arbbot.limit_remaining}). Need to wait.')
                    main_console.append('Not enough bananas. Need to wait.')
                    continue

                # Create the tentative buy
                base_logger.info('Making tentative buy order and checking sell order book...')
                main_console.append('Making tentative buy order and checking sell order book...')
                buy_dollar_amount = arbbot.get_buy_dollar_amount(diff)
                buy_order = arbbot.create_buy_order(buy_dollar_amount)
                coins_to_transact = float(buy_order['amount']['amount'])
                base_logger.debug(f'buy_dollar_amount={buy_dollar_amount} coins_to_transact={coins_to_transact}')

                # Check the sell order book again
                net_sell_price, gross_sell_price = arbbot.get_net_sell_price(coins_to_transact)
                base_logger.debug(f'net_sell_price={net_sell_price} gross_sell_price={gross_sell_price}')

                # Double check that we're still above minimum profit margin
                if 1 - buy_dollar_amount / net_sell_price >= arbbot.get_min_profit_percent():
                    base_logger.info('Exact amounts are still valid')
                    main_console.append('Exact amounts are still valid!')

                    # Selling corresponding ETH amount
                    try:
                        base_logger.info('Making sell order...')
                        main_console.append('Making sell order...')
                        if settings.TEST_MODE:
                            base_logger.info('...But we\'re in Test Mode so not really')
                            sell_order = arbbot.create_sell_order_test(coins_to_transact, gross_sell_price_per_coin)
                        else:
                            sell_order = arbbot.create_sell_order(coins_to_transact, gross_sell_price_per_coin)
                        gross_sell_price, sell_fee = Arbbot.get_gross_price_from_sell_order(sell_order)
                        net_sell_price = gross_sell_price - sell_fee
                        base_logger.debug(f'gross_sell_price={gross_sell_price} sell_fee={sell_fee} '
                                          f'net_sell_price={net_sell_price}')
                    except TimeoutError:
                        # Timeout while getting sell order details
                        base_logger.warning('Timed out while getting sell order details. Will continue.')
                        main_console.append('Timed out while getting sell order details. Will continue.')

                    # Committing the buy order
                    try:
                        base_logger.info('Committing the buy...')
                        main_console.append('Committing the buy...')
                        if not settings.TEST_MODE:
                            buy_order = arbbot.commit_buy_order()
                        else:
                            base_logger.info('...But we\'re in Test Mode so not really')
                    except Exception as e:
                        # Our buy order has expired, but we're going to try make a new one
                        base_logger.exception(e)
                        base_logger.info('Trying to buy again...')
                        main_console.append('Trying to buy again...')
                        if not settings.TEST_MODE:
                            buy_order = arbbot.create_buy_order(buy_dollar_amount, commit='true')
                        else:
                            base_logger.info('...But we\'re in Test Mode so not really')

                    final_buy_amount = float(buy_order['amount']['amount'])
                    base_logger.debug(f'final_buy_amount={final_buy_amount}')

                    # Print and push messages
                    if net_sell_price:
                        push_message = f'Buy: ${buy_dollar_amount:.2f}\nProfit: ' \
                                       f'${net_sell_price - buy_dollar_amount:.2f} ' \
                                       f'({(net_sell_price - buy_dollar_amount) / buy_dollar_amount * 100:.2f}%)'
                        tx_logger.info(f'${buy_dollar_amount:.2f},${net_sell_price:.2f},'
                                       f'${net_sell_price - buy_dollar_amount:.2f},'
                                       f'{(net_sell_price - buy_dollar_amount) / buy_dollar_amount * 100:.2f}%')
                        if settings.TEST_MODE:
                            tx_logger.info('^ This was a test')
                        main_console.append(f'{datetime.utcnow().isoformat(timespec="milliseconds")}Z '
                                            f'[Performed Action] Buy: ${buy_dollar_amount:8.2f} | '
                                            f'Sell: ${net_sell_price:8.2f} | '
                                            f'Profit: ${net_sell_price - buy_dollar_amount:7.2f} '
                                            f'({(net_sell_price - buy_dollar_amount) / buy_dollar_amount * 100:5.2f}%)')

                    # Print in different format if we do not have sell price
                    else:
                        push_message = f'Buy: ${buy_dollar_amount:.2f}\nProfit: $N/A (N/A%)'
                        tx_logger.info(f'${buy_dollar_amount:.2f},N/A,N/A')
                        if settings.TEST_MODE:
                            tx_logger.info('^ This was a test')
                        main_console.append(f'{datetime.utcnow().isoformat(timespec="milliseconds")}Z '
                                            f'[Performed Action] Buy: ${buy_dollar_amount:8.2f} | '
                                            f'Sell: $N/A | Profit: N/A')

                    # If the amount of coins bought/sold is not the same (happens when buy times out)
                    if coins_to_transact != final_buy_amount:
                        push_message += f'\nCoin Diff: {final_buy_amount - coins_to_transact}'
                        base_logger.warning(f'Coin discrepancy. CoinBuy: {final_buy_amount} | '
                                            f'CoinSell: {coins_to_transact} | '
                                            f'Diff: {final_buy_amount - coins_to_transact}')
                        main_console.append(f'{datetime.utcnow().isoformat(timespec="milliseconds")}Z [Discrepancy] '
                                            f'CoinBuy: {final_buy_amount} | CoinSell: {coins_to_transact} | '
                                            f'Diff: {final_buy_amount - coins_to_transact}')

                    base_logger.info('Sending to pushbullet')
                    push(push_message)

                    # Update limits and fees
                    base_logger.info('Updating limits/fees after action')
                    arbbot.update_buy_limits()
                    arbbot.update_bm_sell_fee()

                    # Quit after a single buy/sell
                    if settings.ACTION_ONCE:
                        base_logger.debug('ACTION_ONCE is set')
                        raise KeyboardInterrupt

                else:
                    # If no profit, just cut our losses
                    base_logger.info('Real numbers below threshold. Bailing...')
                    main_console.append('Real numbers below threshold. Bailing...')

            # Reset error count when successful check is done
            error_count = 0

            # Keep main_console and errors_console under 200 length
            if len(main_console) > 200:
                main_console = main_console[-200:]

            if len(errors_console):
                errors_console = errors_console[-200:]
        except KeyboardInterrupt:
            exit_gracefully()
        except Exception as e:
            error_count += 1
            base_logger.debug(f'Error count is: {error_count}')
            base_logger.exception(e)
            errors_console.append(f'{datetime.utcnow().isoformat(timespec="milliseconds")}Z [ERROR] {e}')
            try:
                push(f'[ERROR] {e}')
            except Exception:
                pass
        finally:
            try:
                # Exit if too many errors_console
                if error_count >= 3:
                    base_logger.error('Max error count reached. Exiting...')
                    return

                with open(settings.LOGGING['handlers']['transaction']['filename'], 'r') as fh:
                    transactions = fh.readlines()
                status = {'test_mode': settings.TEST_MODE, 'limit': arbbot.limit_remaining,
                          'limit_total': arbbot.limit_total, 'buy_at': arbbot.get_min_profit_percent()}
                draw_screen(screen, main_console, transactions, errors_console, status)
                # Check if we have to wait before next check
                time_to_sleep = settings.CHECK_INTERVAL - (datetime.utcnow() - start_time).total_seconds()
                if time_to_sleep > 0:
                    base_logger.debug(f'Sleeping for {time_to_sleep} seconds')
                    sleep(time_to_sleep)

            except KeyboardInterrupt:
                exit_gracefully()


if __name__ == '__main__':
    main()
