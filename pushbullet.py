import requests
import settings


def push(message):
    body = {'body': message, 'title': 'Arbbot', 'type': 'note'}
    for token in settings.PUSHBULLET_TOKENS:
        headers = {'Access-Token': token}
        requests.post('https://api.pushbullet.com/v2/pushes', json=body, headers=headers)
